//
//  CarouselUITests.swift
//  CarouselUITests
//
//  Created by Juan Carlos Docal Yanez on 6/9/18.
//  Copyright © 2018 Juan Carlos Docal Yanez. All rights reserved.
//

import XCTest
import CarouselUI

extension CarouselUITests: CarouselDelegate {

    func fetchImages(completion: @escaping ([UIImageView]?) -> Void) {
        expectation.fulfill()
    }
}

class CarouselUITests: XCTestCase {

    let expectation = XCTestExpectation(description: "It fetches the initial batch of images upon creation")

    func testInitialFetchRequest() {
        let _: CarouselUIAPI = CarouselUIFactory().create(self)
        wait(for: [expectation], timeout: 2.0)
    }
}
