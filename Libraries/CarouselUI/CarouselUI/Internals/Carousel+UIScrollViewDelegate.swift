//
//  Carousel+UIScrollViewDelegate.swift
//  CarouselUI
//
//  Created by Juan Carlos Docal Yanez on 6/9/18.
//  Copyright © 2018 Juan Carlos Docal Yanez. All rights reserved.
//

import UIKit

private enum CarouselFetch {
    static let threshold: Int = 2
}

extension Carousel: UIScrollViewDelegate {

    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex: Int = Int(round(scrollView.contentOffset.x / frame.width))

        if currentIndex != pageIndex {
            currentIndex = pageIndex
            if pageIndex == images.count - CarouselFetch.threshold {
                populateCarousel()
            }
        }
    }
}
