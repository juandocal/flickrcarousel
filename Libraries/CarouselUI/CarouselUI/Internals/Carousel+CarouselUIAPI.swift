//
//  Carousel+CarouselUIAPI.swift
//  CarouselUI
//
//  Created by Juan Carlos Docal Yanez on 6/9/18.
//  Copyright © 2018 Juan Carlos Docal Yanez. All rights reserved.
//

import UIKit

extension Carousel: CarouselUIAPI {

    public func view() -> UIView {
        print("\(type(of: self)): \(#function)")
        return self
    }
}
