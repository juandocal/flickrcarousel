//
//  Carousel.swift
//  CarouselUI
//
//  Created by Juan Carlos Docal Yanez on 6/9/18.
//  Copyright © 2018 Juan Carlos Docal Yanez. All rights reserved.
//

import UIKit
import SnapKit

internal class Carousel: UIScrollView {

    weak internal var carouselDelegate: CarouselDelegate?
    internal var images: [UIImageView] = []
    internal var currentIndex: Int = 0
    private let loadingView: UIView = UIView(frame: CGRect.zero)

    internal init (_ delegate: CarouselDelegate) {
        carouselDelegate = delegate
        super.init(frame: CGRect.zero)
        prepareCarousel()
        prepareLoadingView()
        populateCarousel()
    }

    internal required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    internal override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        layoutIfNeeded()

        contentOffset = CGPoint.zero
        // +1 due to the loading view at the end of the carousel.
        let imagesCount = self.images.count + 1
        updateContentSize(imagesCount: imagesCount)
    }

    // MARK: - Private

    private func prepareCarousel() {
        print("\(type(of: self)): \(#function)")
        delegate = self
        isPagingEnabled = true
        showsHorizontalScrollIndicator = false
        showsVerticalScrollIndicator = false
    }

    private func prepareLoadingView() {
        let style: UIActivityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: style)
        activityIndicator.startAnimating()

        loadingView.addSubview(activityIndicator)
        activityIndicator.snp.makeConstraints { (make) in
            make.center.equalTo(loadingView)
            make.size.equalTo(activityIndicator)
        }

        addSubview(loadingView)
        loadingView.snp.makeConstraints({ (make) in
            make.center.size.equalTo(self)
        })
    }

    internal func populateCarousel() {
        print("\(type(of: self)): \(#function)")
        carouselDelegate?.fetchImages(completion: {[weak self] (fetchedImages) in
            print("\(type(of: self)): \(#function)")

            guard let `self` = self else {
                print("\(#function) Error. Self released!")
                return
            }

            guard let fetchedImages: [UIImageView] = fetchedImages, fetchedImages.count > 0 else {
                print("\(type(of: self)): \(#function) Empty list. No need to update layout")
                return
            }

            // +1 due to the loading view at the end of the carousel.
            let imagesCount = self.images.count + fetchedImages.count + 1
            self.updateContentSize(imagesCount: imagesCount)

            var previousImageView: UIImageView? = self.images.last
            for image: UIImageView in fetchedImages {
                self.addSubview(image)
                if let previousImageView: UIImageView = previousImageView {
                    image.snp.makeConstraints({ (make) in
                        make.left.equalTo(previousImageView.snp.right)
                        make.top.size.equalTo(self)
                    })
                } else {
                    image.snp.makeConstraints({ (make) in
                        make.left.top.size.equalTo(self)
                    })
                }
                previousImageView = image
            }
            self.images += fetchedImages

            // Update loading view constraints to be at the end of the pipe.
            if let previousImageView: UIImageView = previousImageView {
                self.loadingView.snp.remakeConstraints({ (make) in
                    make.left.equalTo(previousImageView.snp.right)
                    make.top.size.equalTo(self)
                })
            }
        })
    }

    private func updateContentSize(imagesCount: Int) {
        print("\(type(of: self)): \(#function)")
        let targetWidth: CGFloat = self.frame.width * CGFloat(imagesCount)
        self.contentSize = CGSize(width: targetWidth, height: self.frame.height)
    }
}
