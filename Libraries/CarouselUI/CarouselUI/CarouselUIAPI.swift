//
//  CarouselUIAPI.swift
//  CarouselUI
//
//  Created by Juan Carlos Docal Yanez on 6/9/18.
//  Copyright © 2018 Juan Carlos Docal Yanez. All rights reserved.
//

import UIKit

/// Framework defining the Carousel UI API.

/// Protocol defining the entry point to the `CarouselUI` Framework.
///
/// - Usage: `let carouselUIAPI: CarouselUIAPI = CarouselUIFactory().create(self)`.
public protocol CarouselUIAPI {

    /// View represeting the carousel.
    ///
    /// - Returns: The created carousel view.
    func view() -> UIView
}

/// Protocol defining the carousel delegate.
public protocol CarouselDelegate: class {

    /// Informs the object conforming to `CarouselDelegate` about the need for more images.
    ///
    /// - Parameter completion: callback being invoked on the main thread with an optional list of image views
    ///                         and an optional error.
    func fetchImages(completion: @escaping ([UIImageView]?) -> Void)
}

/// Public factory of objects returning internal instances conforming to different access of the API.
public struct CarouselUIFactory {

    public init () { }

    /// Creates an object conforming to the entry protocol `CarouselUIAPI`
    ///
    /// - Parameter delegate: Object conforming to the `CarouselDelegate` protocol.
    ///
    /// - Returns: The created object of type `CarouselUIAPI`.
    public func create(_ delegate: CarouselDelegate) -> CarouselUIAPI {
        return Carousel(delegate)
    }
}
