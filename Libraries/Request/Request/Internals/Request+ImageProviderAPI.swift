//
//  Request+ImageProviderAPI.swift
//  Request
//
//  Created by Juan Carlos Docal Yanez on 6/9/18.
//  Copyright © 2018 Juan Carlos Docal Yanez. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SnapKit

private enum RequestEndpoint {
    static let key: String = "3eddaf4091523136de10dfa829d29643"
    static let params: String = "api_key=\(key)&text=kittens&format=json&nojsoncallback=1&per_page=3"
    static let endpoint: String = "https://api.flickr.com/services/rest/?method=flickr.photos.search&\(params)"
}

extension Request: ImageProviderAPI {

    public func fetchImages(completion: @escaping ([UIImageView]?, Error?) -> Void) {
        print("\(type(of: self)): \(#function)")
        queue.async {
            Alamofire.request(RequestEndpoint.endpoint).responseJSON(queue: self.queue,
                                                                     completionHandler: {[weak self] (response) in
                print("\(type(of: self)): \(#function)")

                guard let `self` = self else {
                    print("\(#function) Error. Self released!")
                    return
                }

                guard response.result.error == nil else {
                    let description: String = "Error calling GET on \(RequestEndpoint.endpoint)"
                    let error = NSError(domain: "Request", code: 0, userInfo: [ NSLocalizedDescriptionKey: description])
                    DispatchQueue.main.async {
                        completion(nil, error)
                    }
                    return
                }

                guard let json = response.result.value else {
                    let description: String = "Error getting result on \(RequestEndpoint.endpoint)"
                    let error = NSError(domain: "Request", code: 0, userInfo: [ NSLocalizedDescriptionKey: description])
                    DispatchQueue.main.async {
                        completion(nil, error)
                    }
                    return
                }

                let swiftyJson = JSON(json)
                var fetchResult: [UIImageView] = []
                for photo in swiftyJson["photos"]["photo"] {
                    let data: Data? = self.generateData(identifier: photo.1["id"].string,
                                                        farm: photo.1["farm"].int,
                                                        server: photo.1["server"].string,
                                                        secret: photo.1["secret"].string)

                    if let data: Data = data {
                        DispatchQueue.main.async {
                            let imageView: UIImageView = self.processImage(data: data, title: photo.1["title"].string)
                            fetchResult.append(imageView)
                        }
                    }
                }

                DispatchQueue.main.async {
                    completion(fetchResult, nil)
                }
            })
        }
    }

    // MARK: - Private

    private func generateData(identifier: String?,
                              farm: Int?,
                              server: String?,
                              secret: String?) -> Data? {

        print("\(type(of: self)): \(#function)")

        guard let identifier: String = identifier else {
            print("\(type(of: self)): \(#function) Invalid identifier.")
            return nil
        }

        guard let farm: Int = farm else {
            print("\(type(of: self)): \(#function) Invalid farm.")
            return nil
        }

        guard let server: String = server else {
            print("\(type(of: self)): \(#function) Invalid server.")
            return nil
        }

        guard let secret: String = secret else {
            print("\(type(of: self)): \(#function) Invalid secret.")
            return nil
        }

        print("https://farm\(farm).static.flickr.com/\(server)/\(identifier)_\(secret).jpg")
        let generatedUrl = URL(string: "https://farm\(farm).static.flickr.com/\(server)/\(identifier)_\(secret).jpg")
        guard let url: URL = generatedUrl else {
            print("\(type(of: self)): \(#function) Error generating URL \(String(describing: generatedUrl))")
            return nil
        }

        let dataUrl = try? Data(contentsOf: url)
        guard let data: Data = dataUrl else {
            print("\(type(of: self)): \(#function) Error getting data from URL \(String(describing: dataUrl))")
            return nil
        }

        return data
    }

    private func processImage(data: Data, title: String?) -> UIImageView {
        print("\(type(of: self)): \(#function)")

        let imageView: UIImageView = UIImageView(image: UIImage(data: data))
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        imageView.clipsToBounds = true
        if let title: String = title {
            let label: UILabel = UILabel(frame: CGRect.zero)
            label.backgroundColor = UIColor.white
            label.alpha = 0.5
            label.text = title
            imageView.addSubview(label)
            label.snp.makeConstraints { (make) in
                make.left.bottom.right.equalTo(imageView)
                make.height.equalTo(30.0)
            }
        }
        return imageView
    }
}
