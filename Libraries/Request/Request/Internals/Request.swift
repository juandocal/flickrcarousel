//
//  Request.swift
//  Request
//
//  Created by Juan Carlos Docal Yanez on 6/9/18.
//  Copyright © 2018 Juan Carlos Docal Yanez. All rights reserved.
//

import Foundation

internal class Request {

    internal let queue: DispatchQueue

    internal init(_ queue: DispatchQueue) {
        self.queue = queue
    }
}
