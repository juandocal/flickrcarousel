//
//  RequestAPI.swift
//  Request
//
//  Created by Juan Carlos Docal Yanez on 6/9/18.
//  Copyright © 2018 Juan Carlos Docal Yanez. All rights reserved.
//

import Foundation
import UIKit

/// Framework defining the Image Provider's requests API.

/// Protocol defining the entry point to the `Request` Framework.
///
/// - Usage: `let imageProvider: ImageProviderAPI = ImageProviderFactory().create(DispatchQueue.global())`.
public protocol ImageProviderAPI {

    /// Informs the object conforming to `RequestLoginAPI` about the login request.
    ///
    /// - Parameter completion: callback being invoked on the main thread with an optional list of image views
    ///                         and an optional error.
    func fetchImages(completion: @escaping ([UIImageView]?, Error?) -> Void)
}

/// Public factory of objects returning internal instances conforming to different access of the API.
public struct ImageProviderFactory {

    public init () { }

    /// Creates an object conforming to the entry protocol `ImageProviderAPI`
    ///
    /// - Parameter queue: queue on which the request will be performed. Main queue if undefined.
    ///
    /// - Returns: The created object of type `ImageProviderAPI`.
    public func create(_ queue: DispatchQueue = DispatchQueue.main) -> ImageProviderAPI {
        return Request(queue)
    }
}
