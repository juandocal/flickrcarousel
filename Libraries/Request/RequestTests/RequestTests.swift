//
//  RequestTests.swift
//  RequestTests
//
//  Created by Juan Carlos Docal Yanez on 6/9/18.
//  Copyright © 2018 Juan Carlos Docal Yanez. All rights reserved.
//

import XCTest
import Request

class RequestTests: XCTestCase {
    
    func testMainThreadResult() {
        let expectation = XCTestExpectation(description: "It present fetching results on main queue")
        let imageProvider: ImageProviderAPI = ImageProviderFactory().create(DispatchQueue.global())
        imageProvider.fetchImages { (views, error) in
            XCTAssertTrue(Thread.isMainThread)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5.0)
    }
}
