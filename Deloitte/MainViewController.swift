//
//  MainViewController.swift
//  Deloitte
//
//  Created by Juan Carlos Docal Yanez on 6/9/18.
//  Copyright © 2018 Juan Carlos Docal Yanez. All rights reserved.
//

import UIKit
import Request
import CarouselUI

public class MainViewController: UIViewController {

    lazy internal var imageProvider: ImageProviderAPI = ImageProviderFactory().create(DispatchQueue.global())
    lazy internal var carouselProvider: CarouselUIAPI = CarouselUIFactory().create(self)

    public override func viewDidLoad() {
        view.backgroundColor = UIColor.black
        navigationController?.setNavigationBarHidden(true, animated: false)
        prepareLayout()
        super.viewDidLoad()
    }

    public override var prefersStatusBarHidden: Bool {
        return true
    }

    // MARK: - Private

    private func prepareLayout() {
        let carouselView: UIView = carouselProvider.view()
        view.addSubview(carouselView)
        carouselView.snp.makeConstraints { (make) in
            make.edges.equalTo(view)
        }
    }
}
