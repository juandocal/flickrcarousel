//
//  MainViewController+CarouselDelegate.swift
//  Deloitte
//
//  Created by Juan Carlos Docal Yanez on 6/9/18.
//  Copyright © 2018 Juan Carlos Docal Yanez. All rights reserved.
//

import Request
import CarouselUI
import UIKit

extension MainViewController: CarouselDelegate {

    public func fetchImages(completion: @escaping ([UIImageView]?) -> Void) {
        print("\(type(of: self)): \(#function)")
        imageProvider.fetchImages {[weak self] (images, error) in
            print("\(type(of: self)): \(#function)")

            guard let `self` = self else {
                print("\(#function) Error. Self released!")
                return
            }

            guard error == nil else {
                print("\(type(of: self)): \(#function) Error \(String(describing: error))")
                completion(nil)
                return
            }

            completion(images)
        }
    }
}
